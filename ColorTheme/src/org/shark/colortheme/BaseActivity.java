package org.shark.colortheme;

import java.util.HashMap;

import org.shark.theme.util.ActivityHelper;

import android.support.v4.app.FragmentActivity;

public class BaseActivity extends FragmentActivity {
	private HashMap<String, String> caches = new HashMap<String, String>();

	protected String getCacheValue(String key) {
		return caches.get(key);
	}

	protected void putCache(String key, String value) {
		if (key != null) {
			caches.put(key, value);
		}
	}

	protected void removeCache(String key) {
		if (key != null) {
			caches.remove(key);
		}
	}

	private void clearCache() {
		caches.clear();
	}

	final ActivityHelper mActivityHelper = ActivityHelper.createInstance(this);

	protected ActivityHelper getActivityHelper() {
		return mActivityHelper;
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			clearCache();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
