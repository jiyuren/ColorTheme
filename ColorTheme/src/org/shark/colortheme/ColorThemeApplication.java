package org.shark.colortheme;

import java.io.File;

import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.core.ColorThemeManager;
import org.shark.colortheme.core.StyleObserver;
import org.shark.colortheme.core.ThemePackage;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * 项目apk主入口，主题管理器实例寄存宿主
 * @author hljdrl@gmail.com
 *
 */
public class ColorThemeApplication extends Application {
	private static ColorThemeApplication instance = null;
	private ColorThemeManager colorThemeManager;

	/**
	 * 静态工厂方法
	 */
	public synchronized static ColorThemeApplication getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		//初始化
		colorThemeManager = ColorThemeManager.getInstance();
		colorThemeManager.init(this);
		//初始化主题APK中的RES-ID
		ThemePackage.setActivityBackgroundResId(R.drawable.activity_background);
		ThemePackage.setThemeNameResId(R.string.theme_pkg_name_id);
	}
	
	public void addStyleObserver(StyleObserver obs) {
		if (obs != null) {
			colorThemeManager.addStyleObserver(obs);
		}
	}

	public void deleteStyleObserver(StyleObserver obs) {
		if (obs != null) {
			colorThemeManager.deleteStyleObserver(obs);
		}
	}
	
	public void setColorStyle(ColorStyle colorStyle){
		colorThemeManager.setColorStyle(colorStyle);
	}
	public void setActivityBackground(File imageFile){
		colorThemeManager.setActivityBackground(imageFile);
	}
	public void setActivityBackground(String imageFile){
		colorThemeManager.setActivityBackground(imageFile);
	}
	public void setActivityBackground(Drawable drawable){
		colorThemeManager.setActivityBackground(drawable);
	}
	/**
	 * @param uri
	 * @param save 保存uri数据
	 */
	public void setActivityBackgroundAsMedia(Uri uri,boolean save){
		colorThemeManager.setActivityBackgroundAsMedia(uri,save);
	}
	public void setActivityBackground(Bitmap _bitmap){
		colorThemeManager.setActivityBackground(_bitmap);
	}
	
	public void addThemePackage(ThemePackage add){
		colorThemeManager.addThemePackage(add);
	}
	public void removeThemePackage(ThemePackage remove){
		colorThemeManager.deleteThemePackage(remove);
	}
	
	public ColorStyle getColorStyle(){
		return colorThemeManager.getColorStyle();
	}
}
