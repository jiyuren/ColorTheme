package org.shark.colortheme;

import org.shark.colortheme.adapter.TitlePageAdapter;
import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.core.ColorStyleObservable;
import org.shark.colortheme.core.ColorStyleObserver;
import org.shark.colortheme.core.ColorThemeManager;
import org.shark.colortheme.core.ImageStyle;
import org.shark.colortheme.core.ImageStyleObserver;
import org.shark.colortheme.view.PagerSlidingTabStrip;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

/**
 * 主页面
 * 
 * @author hljdrl@gmail.com
 * 
 */
public class HomeActivity extends BaseActivity {
	public static final String[] TITLES = { "图片", "颜色", "APK主题加载器", "控件", "设置",
			"关于" };
	private TitlePageAdapter pageAdapter;
	private PagerSlidingTabStrip tabs;
	private ViewPager mPager;
	private long exitTime = 0;
	private ColorStyle colorStyle;
	private Drawable oldBackground;
	private String TAG = "HomeActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setBackgroundDrawableResource(
				R.drawable.activity_background);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root_home);
		colorStyle = ColorThemeApplication.getInstance().getColorStyle();
		setupRootLayout();
		ColorThemeManager.getInstance().setupActivityBackground(this);

	}

	private void setupRootLayout() {
		setupAction();
		setupView();
	}

	private void setupView() {
		pageAdapter = new TitlePageAdapter(getSupportFragmentManager(), TITLES);
		tabs = (PagerSlidingTabStrip) findViewById(R.id.home_tabStrip);
		mPager = (ViewPager) findViewById(R.id.home_viewpager);
		mPager.setAdapter(pageAdapter);
		tabs.setViewPager(mPager);
		tabs.setTabBackground(colorStyle);
		changeColor(Color.parseColor(colorStyle.getColor()));
		ColorThemeApplication.getInstance().addStyleObserver(
				mColorStyleObserver);
		ColorThemeApplication.getInstance().addStyleObserver(
				mImageStyleObserver);
	}

	private void setupAction() {
		getActivityHelper().setActionBarTitle(R.string.app_name);
	}

	private ColorStyleObserver mColorStyleObserver = new ColorStyleObserver() {
		@Override
		public void onChageColorStyle(ColorStyleObservable o, ColorStyle chage) {
			tabs.setTabBackground(chage);
		}

	};

	public void changeColor(int newColor) {
		tabs.setIndicatorColor(newColor);
		Drawable colorDrawable = new ColorDrawable(newColor);
		if (oldBackground == null) {
			getActivityHelper().setActionBarBackground(newColor);
		} else {
			TransitionDrawable td = new TransitionDrawable(new Drawable[] {
					oldBackground, colorDrawable });
			getActivityHelper().setActionBarBackground(newColor);
			td.startTransition(200);
		}
		oldBackground = colorDrawable;
	}

	private ImageStyleObserver mImageStyleObserver = new ImageStyleObserver() {
		@Override
		public void onChageImageStyle(ColorStyleObservable o, ImageStyle chage) {
			if (chage != null) {
				if (chage.getStyleName() == ImageStyle.IMAGE_STYLE_ACTIVITY_BG) {
					if (chage.getDrawable() != null) {
						getWindow().setBackgroundDrawable(chage.getDrawable());
						Log.d(TAG, "Activity背景已设置");
						Toast.makeText(HomeActivity.this, "Activity背景已设置",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
	};

	private void back() {
		if ((System.currentTimeMillis() - exitTime) > 3000) {
			Toast.makeText(getApplicationContext(), "再次按返回键退出界面!",
					Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == SELECT_PIC) {
			Uri uri = data.getData();
			Log.e("uri", uri.toString());
			ColorThemeApplication.getInstance().setActivityBackgroundAsMedia(uri,true);

		} else if (resultCode == RESULT_OK && requestCode == SELECT_PIC_KITKAT) {
			Uri uri = data.getData();
			Log.e("uri", uri.toString());
			ColorThemeApplication.getInstance().setActivityBackgroundAsMedia(uri,true);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private static final int SELECT_PIC = 123;
	private static final int SELECT_PIC_KITKAT = 321;

	@TargetApi(Build.VERSION_CODES.KITKAT)
	public static void openSystemImages(Activity aty) {
		Intent intent = new Intent();
		/* 开启Pictures画面Type设定为image */
		/* 使用Intent.ACTION_GET_CONTENT这个Action */
		intent.setAction(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/jpeg");
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
			aty.startActivityForResult(intent, SELECT_PIC_KITKAT);
		} else {
			intent.setAction(Intent.ACTION_GET_CONTENT);
			aty.startActivityForResult(intent, SELECT_PIC);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			back();
		}
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ColorThemeApplication.getInstance().deleteStyleObserver(
				mColorStyleObserver);
		ColorThemeApplication.getInstance().deleteStyleObserver(
				mImageStyleObserver);
	}

}
