package org.shark.colortheme.adapter;

public class ColorItems {

	public int color = -1;

	public ColorItems() {
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

}
