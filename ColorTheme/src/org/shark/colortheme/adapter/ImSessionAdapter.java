package org.shark.colortheme.adapter;

import java.util.List;
import org.shark.colortheme.R;
import org.shark.colortheme.im.ImSessionItem;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class ImSessionAdapter extends ArrayAdapter<ImSessionItem> {

	LayoutInflater in =null;
	public ImSessionAdapter(Context context,List<ImSessionItem> objects) {
		super(context, 0, objects);
		in = LayoutInflater.from(context);
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View mVm, ViewGroup parent) {
		if(mVm==null){
			mVm = in.inflate(R.layout.item_session, null);
		}
		
		return mVm;
	}

}
