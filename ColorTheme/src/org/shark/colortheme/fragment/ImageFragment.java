package org.shark.colortheme.fragment;

import org.shark.colortheme.ColorThemeApplication;
import org.shark.colortheme.HomeActivity;
import org.shark.colortheme.ImChatActivity;
import org.shark.colortheme.R;
import org.shark.colortheme.core.AbsImageLoader;
import org.shark.colortheme.core.ColorThemeManager;
import org.shark.colortheme.core.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class ImageFragment extends Fragment implements  OnClickListener {
	private View rootView;

	private ImageView mImageView1;
	private ImageView mImageView2;
	private ImageView mImageView3;
	private ImageView mImageView4;
	private ImageView mImageView5;
	private ImageView mImageView6;
	private Button mBtnFileImage;
	
	private Button mBtnToChat;
	public static ImageFragment newInstance() {
		ImageFragment fragment = new ImageFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_image, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		mBtnFileImage = (Button) rootView.findViewById(R.id.set_file_iamge);
		mImageView1 = (ImageView) rootView.findViewById(R.id.ft_ImageView01);
		mImageView2 = (ImageView) rootView.findViewById(R.id.ft_ImageView02);
		mImageView3 = (ImageView) rootView.findViewById(R.id.ft_ImageView03);
		mImageView4 = (ImageView) rootView.findViewById(R.id.ft_ImageView04);
		mImageView5= (ImageView) rootView.findViewById(R.id.ft_ImageView05);
		mImageView6 = (ImageView) rootView.findViewById(R.id.ft_ImageView06);
		
		mBtnToChat = (Button) rootView.findViewById(R.id.set_to_chat);

		mImageView1.setOnClickListener(this);
		mImageView2.setOnClickListener(this);
		mImageView3.setOnClickListener(this);
		mImageView4.setOnClickListener(this);
		mImageView5.setOnClickListener(this);
		mImageView6.setOnClickListener(this);
		mBtnFileImage.setOnClickListener(this);
		mBtnToChat.setOnClickListener(this);

	}
	
	

	@Override
	public void onClick(View v) {
		AbsImageLoader imageLoader = ColorThemeManager.getInstance().getImageLoader();
		switch (v.getId()) {
		
		case R.id.set_to_chat:
			Intent mIm = new Intent(getActivity(),ImChatActivity.class);
			getActivity().startActivity(mIm);
			break;
		case R.id.ft_ImageView01:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.img_article_bg_red));
			break;
		case R.id.ft_ImageView02:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.img_article_bg_green));

			break;
		case R.id.ft_ImageView03:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.activity_background));

			break;
		case R.id.ft_ImageView04:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.img_article_bg_yellow));

			break;
		case R.id.ft_ImageView05:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.activity_bg_5));
			break;
		case R.id.ft_ImageView06:
			ColorThemeApplication.getInstance().setActivityBackground(
					imageLoader.loadResourcesDrawable(getActivity(),
							R.drawable.activity_bg_6));
			break;
		case R.id.set_file_iamge:
			HomeActivity.openSystemImages(getActivity()); 
			break;
		}

	}



}
