package org.shark.colortheme.im;

import android.graphics.drawable.Drawable;

public class ImSessionItem {
	private String name="";
	private int messageCount = 0;
	private String time = "";
	private Drawable icon = null;
	/**
	 * @return the messageCount
	 */
	public int getMessageCount() {
		return messageCount;
	}
	/**
	 * @param messageCount the messageCount to set
	 */
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}
	/**
	 * @return the icon
	 */
	public Drawable getIcon() {
		return icon;
	}
	/**
	 * @param icon the icon to set
	 */
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

}
