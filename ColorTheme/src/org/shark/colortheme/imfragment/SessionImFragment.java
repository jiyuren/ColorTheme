package org.shark.colortheme.imfragment;

import java.util.ArrayList;
import java.util.List;

import org.shark.colortheme.R;
import org.shark.colortheme.adapter.ImSessionAdapter;
import org.shark.colortheme.im.ImSessionItem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class SessionImFragment extends Fragment {
	private View rootView;
	private ListView lv_session;
	private ImSessionAdapter mImSessionAdapter;
	private List<ImSessionItem> sessions = new ArrayList<ImSessionItem>();
	public static SessionImFragment newInstance() {
		SessionImFragment fragment = new SessionImFragment();
		return fragment;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
		sessions.add(new ImSessionItem());
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_imsession, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		lv_session = (ListView) rootView.findViewById(R.id.lv_session);
		mImSessionAdapter = new ImSessionAdapter(getActivity(), sessions);
		lv_session.setAdapter(mImSessionAdapter);;
	}
}
