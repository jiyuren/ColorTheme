package org.shark.colortheme.service;

import org.shark.colortheme.ColorThemeApplication;
import org.shark.colortheme.core.ThemePackage;
import org.shark.colortheme.util.ThemeUtil;

import android.app.IntentService;
import android.content.Intent;

/**
 * @author hljdrl@gmail.com
 *
 */
public class PackageService extends IntentService {
    static private final String TAG = "PackageService";
	public PackageService() {
		super(TAG);
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent!=null){
			String pkgName = intent.getStringExtra("pkgName");
			String type = intent.getStringExtra("type");
			if(type!=null){
				if(type.equals("add")){
					if(pkgName!=null && pkgName.length()>0){
						ThemePackage mThemePackage = ThemeUtil.loadThemePackage(this, pkgName);
						ColorThemeApplication.getInstance().addThemePackage(mThemePackage);
					}
				}else if(type.equals("del")){
					if(pkgName!=null && pkgName.length()>0){
						ThemePackage mThemePackage = ThemeUtil.loadThemePackage(this, pkgName);
						ColorThemeApplication.getInstance().removeThemePackage(mThemePackage);
					}
				}
			}
			
		}
	}

}
