package org.shark.colortheme.core;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * 图片加载抽象类，主要负责本地图片加载，RES图片加载
 * 
 * @author hljdrl@gmail.com
 * 
 */
public abstract class AbsImageLoader {
	public abstract Drawable loadKitKatMediaAsDrawable(Context ctx, Uri _uri);

	public abstract Drawable loadResourcesDrawable(Context ctx, int resid);

	public abstract void loadPictureDrawable(ImageStyle _image);

	public abstract void loadBitmap(ImageStyle _image);

	public abstract void loadBitmap(ImageStyle _image,
			android.graphics.BitmapFactory.Options opt);
}
