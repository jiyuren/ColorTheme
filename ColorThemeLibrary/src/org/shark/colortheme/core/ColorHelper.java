/**
 * 
 */
package org.shark.colortheme.core;

import android.graphics.Color;

/**
 * @author hljdrl@gmail.com
 * 
 */
public final class ColorHelper {
	public static String convertToDecimal(int color) {
		int alpha = Color.alpha(color);
		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		StringBuffer tmp = new StringBuffer();
		tmp.append("alpha-透明度").append(alpha).append(" ");
		tmp.append(",  red-红:").append(red).append(" ");
		tmp.append(",   green-绿:").append(green).append(" ");
		tmp.append(",    blue-蓝:").append(blue).append(" ");
		return tmp.toString();
	}

	public static int convertColors(int c1, int c2) {
		int a1 = Color.alpha(c1);
		int red = Color.red(c1);
		int green = Color.green(c1);
		int blue = Color.blue(c1);

		int a2 = Color.alpha(c1);
		int red1 = Color.red(c2);
		int green1 = Color.green(c2);
		int blue1 = Color.blue(c2);

		int alpha = (a1 + a2) / 2;
		int r2 = (red + red1) / 2;
		int g2 = (green + green1) / 2;
		int b2 = (blue + blue1) / 2;
		return Color.argb(alpha, r2, g2, b2);

	}

	public static int convertColors3(int c1, int c2, int c3) {
		// int alpha = Color.alpha(c1);
		int red = Color.red(c1);
		int green = Color.green(c1);
		int blue = Color.blue(c1);

		// int alpha = Color.alpha(c1);
		int red1 = Color.red(c2);
		int green1 = Color.green(c2);
		int blue1 = Color.blue(c2);

		int red2 = Color.red(c3);
		int green2 = Color.green(c3);
		int blue2 = Color.blue(c3);

		int r2 = (red + red1 + red2) / 3;
		int g2 = (green + green1 + green2) / 3;
		int b2 = (blue + blue1 + blue2) / 3;

		return Color.rgb(r2, g2, b2);

	}

	/**
	 * For custom purposes. Not used by ColorPickerPreferrence
	 * 
	 * @param color
	 * @author Unknown
	 */
	public static String convertToARGB(int color) {

		String alpha = Integer.toHexString(Color.alpha(color));
		String red = Integer.toHexString(Color.red(color));
		String green = Integer.toHexString(Color.green(color));
		String blue = Integer.toHexString(Color.blue(color));

		if (alpha.length() == 1) {
			alpha = "0" + alpha;
		}

		if (red.length() == 1) {
			red = "0" + red;
		}

		if (green.length() == 1) {
			green = "0" + green;
		}

		if (blue.length() == 1) {
			blue = "0" + blue;
		}

		return "#" + alpha + red + green + blue;
	}

	public static String convertToRGB(int color) {
		String alpha = Integer.toHexString(Color.alpha(color));
		String red = Integer.toHexString(Color.red(color));
		String green = Integer.toHexString(Color.green(color));
		String blue = Integer.toHexString(Color.blue(color));
		StringBuffer tmp = new StringBuffer();
		if (alpha.length() == 1) {
			alpha = "0" + alpha;
		}
		tmp.append("alpha-透明度:").append(alpha).append(" ");
		if (red.length() == 1) {
			red = "0" + red;
		}
		tmp.append(",  red-红:").append(red).append(" ");
		if (green.length() == 1) {
			green = "0" + green;
		}
		tmp.append(",  green-绿:").append(green).append(" ");
		if (blue.length() == 1) {
			blue = "0" + blue;
		}
		tmp.append(",  blue-蓝:").append(blue).append(" ");
		return tmp.toString();
	}

	/**
	 * For custom purposes. Not used by ColorPickerPreferrence
	 * 
	 * @param argb
	 * @throws NumberFormatException
	 * @author Unknown
	 */
	public static int convertToColorInt(String argb)
			throws NumberFormatException {

		if (argb.startsWith("#")) {
			argb = argb.replace("#", "");
		}

		int alpha = -1, red = -1, green = -1, blue = -1;

		if (argb.length() == 8) {
			alpha = Integer.parseInt(argb.substring(0, 2), 16);
			red = Integer.parseInt(argb.substring(2, 4), 16);
			green = Integer.parseInt(argb.substring(4, 6), 16);
			blue = Integer.parseInt(argb.substring(6, 8), 16);
		} else if (argb.length() == 6) {
			alpha = 255;
			red = Integer.parseInt(argb.substring(0, 2), 16);
			green = Integer.parseInt(argb.substring(2, 4), 16);
			blue = Integer.parseInt(argb.substring(4, 6), 16);
		}

		return Color.argb(alpha, red, green, blue);
	}

	public static String convertToPressed(String argb) {
		if (argb.startsWith("#")) {
			argb = argb.replace("#", "");
		}
		int alpha = -1, red = -1, green = -1, blue = -1;

		if (argb.length() == 8) {
			alpha = Integer.parseInt(argb.substring(0, 2), 16);
			red = Integer.parseInt(argb.substring(2, 4), 16) / 2;
			green = Integer.parseInt(argb.substring(4, 6), 16) / 2;
			blue = Integer.parseInt(argb.substring(6, 8), 16) / 2;
		} else if (argb.length() == 6) {
			alpha = 255;
			red = Integer.parseInt(argb.substring(0, 2), 16) / 2;
			green = Integer.parseInt(argb.substring(2, 4), 16) / 2;
			blue = Integer.parseInt(argb.substring(4, 6), 16) / 2;
		}
		int color = Color.argb(alpha, red, green, blue);
		return convertToARGB(color);
	}

	public static String convertToAlpha(String strAlpha, String argb) {
		if (argb.startsWith("#")) {
			argb = argb.replace("#", "");
		}
		int alpha = -1, red = -1, green = -1, blue = -1;

		if (argb.length() == 8) {
			alpha = Integer.parseInt(strAlpha, 16);
			red = Integer.parseInt(argb.substring(2, 4), 16);
			green = Integer.parseInt(argb.substring(4, 6), 16);
			blue = Integer.parseInt(argb.substring(6, 8), 16);
		} else if (argb.length() == 6) {
			alpha = Integer.parseInt(strAlpha, 16);
			red = Integer.parseInt(argb.substring(0, 2), 16);
			green = Integer.parseInt(argb.substring(2, 4), 16);
			blue = Integer.parseInt(argb.substring(4, 6), 16);
		}
		int color = Color.argb(alpha, red, green, blue);
		return convertToARGB(color);
	}
}
