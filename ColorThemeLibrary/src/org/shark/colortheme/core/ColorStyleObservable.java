package org.shark.colortheme.core;


/**
 * @author hljdrl@gmail.com
 *
 */
public interface ColorStyleObservable {
	
	public void addStyleObserver(StyleObserver obs);
	public void deleteStyleObserver(StyleObserver obs);

}
