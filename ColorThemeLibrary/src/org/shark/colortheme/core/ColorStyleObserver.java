package org.shark.colortheme.core;



/**
 * @author hljdrl@gmail.com
 *
 */
public interface ColorStyleObserver extends StyleObserver {
	
	public void onChageColorStyle(ColorStyleObservable o,ColorStyle chage);

}
