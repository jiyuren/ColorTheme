package org.shark.colortheme.core;

/**
 * 主题包回调接口
 * @author hljdrl@gmail.com
 *
 */
public interface PackageThemeObserver extends StyleObserver {
	
	public static final int PACKAGE_THEME_ADD    = 100;
	public static final int PACKAGE_THEME_REMOVE = 101;
	
	/**
	 * 安装主题包
	 * @param pkg
	 */
	void onInstallPackageTheme(ThemePackage theme);
	/**
	 * 卸载主题包
	 * @param pkg
	 */
	void onUnInstallPackageTheme(ThemePackage theme);

}
