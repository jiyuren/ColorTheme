package org.shark.colortheme.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;

public class ContextUtil {
	
	public final static Context finalAppContext(Context ctx,String pkg){
		Context mCm = null;
		try {
			mCm = ctx.createPackageContext(pkg, Context.CONTEXT_IGNORE_SECURITY);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return mCm;
	}
	
	public static final String getApkString(Context ctx,int resid){
		String string ="";
		try{
			string = ctx.getString(resid);
		}catch(NotFoundException not){
			not.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return string;
	}
	public static final Drawable getDrawable(Context ctx,int resid){
		return ctx.getResources().getDrawable(resid);
	}

}
