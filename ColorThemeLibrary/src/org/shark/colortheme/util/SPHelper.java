package org.shark.colortheme.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

/**
 * SharedPreferences帮助类
 * 
 * @author wang.song
 *
 */
public final class SPHelper {
	final public static void setColor(Context context, String color) {
		SharedPreferences sp = context.getSharedPreferences("color",
				Context.MODE_PRIVATE);
		if (sp != null) {
			Editor mEditor = sp.edit();
			mEditor.putString("color", color);
			mEditor.commit();
		}
	}
	
	public static final String getColor(Context context){
		SharedPreferences sp = context.getSharedPreferences("color",
				Context.MODE_PRIVATE);
		return sp.getString("color", "#FF3F9FE0");
	}
	
	final public static void setActivityBackgroundFile(Context context, String color) {
		SharedPreferences sp = context.getSharedPreferences("image_style",
				Context.MODE_PRIVATE);
		if (sp != null) {
			Editor mEditor = sp.edit();
			mEditor.putString("activity_bg_file", color);
			mEditor.commit();
		}
	}
	final public static void saveActivityBackgroundUri(Context context, Uri _uri) {
		SharedPreferences sp = context.getSharedPreferences("image_style",
				Context.MODE_PRIVATE);
		if (sp != null) {
			Editor mEditor = sp.edit();
			mEditor.putString("aty_bg_uri", _uri.toString());
			mEditor.commit();
		}
	}
	final public static Uri readActivityBackgroundUri(Context ctx){
		Uri mUri = null;
		SharedPreferences sp = ctx.getSharedPreferences("image_style",
				Context.MODE_PRIVATE);
		String v = sp.getString("aty_bg_uri", "");
		if(v!=null && !v.equals("")){
			mUri = Uri.parse(v);
		}
		return mUri;
	}
	
	public static final String getActivityBackgroundFile(Context context){
		SharedPreferences sp = context.getSharedPreferences("image_style",
				Context.MODE_PRIVATE);
		return sp.getString("activity_bg_file", "");
	}
}
