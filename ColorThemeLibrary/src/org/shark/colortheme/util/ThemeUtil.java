package org.shark.colortheme.util;

import java.util.ArrayList;
import java.util.List;

import org.shark.colortheme.core.ThemePackage;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;


public class ThemeUtil {

	
	public final static PackageInfo findPacketInfo(Context ctx){
		PackageManager _pm = ctx.getPackageManager();
		PackageInfo _info  = null;
		try {
			_info= _pm.getPackageInfo(ctx.getPackageName(), PackageManager.GET_UNINSTALLED_PACKAGES);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return _info;
	}
	public static final String PACKAGE_NAME = "org.colortheme";
	public final static ThemePackage loadThemePackage(Context ctx,String pkg){
		ThemePackage mThemePackage = null;
		PackageManager pm = ctx.getPackageManager();
		try {
			PackageInfo info = pm.getPackageInfo(pkg, PackageManager.GET_UNINSTALLED_PACKAGES);
			if(info!=null ){
				String sharedUserId = info.sharedUserId;
				if (sharedUserId!=null &&  PACKAGE_NAME.equals(sharedUserId) && !info.packageName.equals("org.shark.colortheme")){
					mThemePackage = new ThemePackage();
					mThemePackage.setPkgName(pkg);
				}
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return mThemePackage;
	}
	public final static List<ThemePackage> loadAllThemePackage(Context ctx) {
		
		List<ThemePackage> plugins = new ArrayList<ThemePackage>();
		PackageManager pm = ctx.getPackageManager();
		List<PackageInfo> pkgs = pm.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);
		for (PackageInfo pkg : pkgs) {
			// 包名
			String sharedUserId = pkg.sharedUserId;
			String pkgName = pkg.packageName;
			// sharedUserId是开发时约定好的，这样判断是否为自己人
			if (pkgName.equals("org.shark.colortheme") || sharedUserId==null || !PACKAGE_NAME.equals(sharedUserId)){
				continue;
			}else{
				ThemePackage plug = new ThemePackage();
				plug.setPkgName(pkg.packageName);
				plugins.add(plug);
			}
			
		}

		return plugins;

	}

}
